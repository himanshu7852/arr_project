function filter(elements,cb)
{
    if(!Array.isArray(elements))
    {
        return;
    }
    let resArray=[];
    for(let index=0;index<elements.length;index++)
    {
        if(cb(elements[index],index,elements)==true)
        {
            resArray.push(elements[index])
        }
    }
    return resArray;
}
module.exports=filter;