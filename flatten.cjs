function flatten(elements,depth)
{
    if(depth===undefined)
    {
        depth=1;
    }

    let resFlatten=[];
    for(let index=0;index<elements.length;index++)
    {
        if(Array.isArray(elements[index]) && depth>0)
        {
            let temp=flatten(elements[index], depth -1)

            resFlatten=resFlatten.concat(temp);
        
        }
        else{
            if(elements[index]!==undefined)
            {
                resFlatten.push(elements[index]);
            }
        }
    }
    return resFlatten;
}

module.exports=flatten;