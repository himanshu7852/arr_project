function map(elements,cb)
{
    if(elements===undefined||elements===null)
    {
        return [];
    }
    let resArray=[];
    if(!Array.isArray(elements))
    {
        return false;
    }
    for(let index=0;index<elements.length;index++)
    {
        let temp=cb(elements[index],index,elements);
        resArray.push(temp);
    }
    return resArray;
}
module.exports=map;