//const items = [1, 2, 3, 4, 5, 5];
function each(elements, cb)
{
    if(!Array.isArray(elements))
    {
        return false;
    }

    for(let index=0;index<elements.length;index++)
    {
        cb(elements[index],index);
    }
}

module.exports=each;