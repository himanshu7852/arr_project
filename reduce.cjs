function reduce(elements,cb,startingValue)
{
    let redIdx=0;
    if(startingValue===undefined)
    {
        startingValue=elements[0];
        redIdx=1;
    }
    for(let index=redIdx;index<elements.length;index++)
    {
        if(elements[index]!==undefined)
        {
            startingValue=cb(startingValue,elements[index],index,elements);
        }
        
    }
    return startingValue;
}

module.exports=reduce;